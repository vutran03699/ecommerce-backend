const dev = {
    app: {
        port: process.env.DEV_APP_PORT || 3000
    },
    db: {
        //mongodb+srv://admin:VKamEYTDN1oHbZ66@ecomemerce.rmzrng6.mongodb.net/?retryWrites=true&w=majority
        urlMongoDB: process.env.DEV_URL_MONGODB || 'mongodb://localhost:27017'
    }
}

const prod = {
    app: {
        port: process.env.PROD_APP_PORT || 3000
    },
    db: {
        urlMongoDB: process.env.PROD_URL_MONGODB || 'mongodb://localhost:27017'
    }
}

const config = {
    dev,
    prod
}

const env = process.env.NODE_ENV || 'dev'

module.exports = config[env]

