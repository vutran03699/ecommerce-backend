'use strict'

const StatusCodes = {
    OK: 200,
    CREATED: 201
}

const ReasonStatusCode = {
    OK: 'OK',
    CREATED: 'CREATED'
}

class SuccessResponse {
    constructor(message, statusCode = StatusCodes.OK, reasonStatusCode = ReasonStatusCode.OK, metadata = {}) {
        this.message = !message ? reasonStatusCode : message
        this.status = statusCode
        this.metadata = metadata
    }

    send(res) {
        res.status(this.status).json(this)
    }

}

class OK extends SuccessResponse {
    constructor({ message, metadata = {} }) {
        super(message, StatusCodes.OK, ReasonStatusCode.OK, metadata)
    }
}

class CREATED extends SuccessResponse {
    constructor({ options = {}, message, statusCode = StatusCodes.CREATED, reasonStatusCode = ReasonStatusCode.CREATED, metadata = {} }) {
        super(message, statusCode, reasonStatusCode, metadata)
        this.options = options
    }
}

module.exports = {
    OK,
    CREATED
}