'use strict'

const mongoose = require('mongoose')
const { db: { urlMongoDB } } = require('../configs/config.mongodb')
console.log(urlMongoDB)
// const connectString = 'mongodb://localhost:27017'
const connectString = urlMongoDB
class Database {
    constructor() {
        this.connect()
    }

    connect() {
        if (1 === 1) {
            mongoose.set('debug', true)
            mongoose.set('debug', { color: true })
        }

        mongoose.connect(connectString, {
            maxPoolSize: 10
        }).then(() => {
            console.log("Connected to DB successfully Pro")
        }).catch(err => {
            console.log("Error connecting to DB", err)
        })
    }

    static getInstance() {
        if (!Database.instance) {
            Database.instance = new Database()
        }
        return Database.instance
    }
}


const instanceMongoDB = Database.getInstance()


module.exports = instanceMongoDB