'use strict'

const jwt = require('jsonwebtoken')

const createTokenPair = async (payload, publicKey, privateKey) => {
    try {
        //access token
        const accessToken = jwt.sign(payload, privateKey, {
            algorithm: 'RS256',
            expiresIn: '2 days'
        });

        //refresh token
        const refreshToken = jwt.sign(payload, privateKey, {
            algorithm: 'RS256',
            expiresIn: '7 days'
        });

        //verify token
        jwt.verify(accessToken, publicKey, (err, decoded) => {
            if (err) {
                console.error(`error verify : ${err}`);
            } else {
                console.log(`decoded verify : ${decoded}`);
            }

        });

        return {
            accessToken,
            refreshToken
        }

    } catch (error) {
        return error
    }

}

module.exports = {
    createTokenPair
}