'use strict'

const mongoose = require('mongoose'); // Erase if already required

// key token Model

const DOCUMENT_NAME = 'KeyToken';
const COLLECTION_NAME = 'KeyTokens';

// Declare the Schema of the Mongo model

var keyTokenSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Shop'
    },
    publicKey: {
        type: String, require: true
    },
    refreshToken: {
        type: Array,
    }
}, {
    timestamps: true,
    collection: COLLECTION_NAME
})

//Export the model
module.exports = mongoose.model(DOCUMENT_NAME, keyTokenSchema);