const app = require('./src/app')

app.listen(3000, () => {
    console.log('Listening on port http://localhost:3000 ')
})