# Dự Án Backend eCommerce

## Mục Tiêu

Xây dựng hệ thống backend cho một trang web thương mại điện tử đơn giản, đáp ứng các yêu cầu cơ bản như đăng ký/đăng nhập người dùng, quản lý sản phẩm, giỏ hàng, đặt hàng và quản lý đơn hàng. Hệ thống được xây dựng theo kiến trúc microservices và triển khai trên AWS.

## Các Mục Todo

### Mục 1: Cài Đặt Môi Trường
- [X] Cài đặt Node.js
- [X] Cài đặt MongoDB
- [X] Cài đặt Mongoose
- [ ] Cài đặt Jest
- [ ] Cài đặt AWS CLI

### Mục 2: Tạo Dự Án
- [X] Tạo thư mục dự án
- [X] Tạo file package.json
- [X] Tạo file .env

### Mục 3: Xây Dựng API Cho Người Dùng
- [X] API Đăng ký người dùng
- [X] API Đăng nhập người dùng
- [ ] API Lấy thông tin người dùng

### Mục 4: Xây Dựng API Cho Sản Phẩm
- [ ] API Lấy danh sách sản phẩm
- [ ] API Lấy chi tiết sản phẩm
- [ ] API Thêm/Sửa/Xóa sản phẩm

### Mục 5: Xây Dựng API Cho Giỏ Hàng
- [ ] API Thêm sản phẩm vào giỏ hàng
- [ ] API Sửa số lượng sản phẩm trong giỏ hàng
- [ ] API Xóa sản phẩm khỏi giỏ hàng
- [ ] API Lấy thông tin giỏ hàng

### Mục 6: Xây Dựng API Cho Đơn Hàng
- [ ] API Tạo đơn hàng
- [ ] API Lấy thông tin đơn hàng
- [ ] API Cập nhật trạng thái đơn hàng

### Mục 7: Tách Dự Án Thành Microservices
- [ ] Tạo microservices cho người dùng
- [ ] Tạo microservices cho sản phẩm
- [ ] Tạo microservices cho giỏ hàng
- [ ] Tạo microservices cho đơn hàng

### Mục 8: Triển Khai Microservices Lên AWS
- [ ] Tạo EC2 instance
- [ ] Cài đặt Node.js trên EC2 instance
- [ ] Cài đặt microservices lên EC2 instance

### Mục 9: Tạo CI/CD Pipeline
- [ ] Tạo repository trên GitHub
- [ ] Tạo pipeline CI/CD trên GitHub Actions


### Yêu Cầu

- Node.js
- MongoDB
- Mongoose
- Jest
- AWS CLI

### Cài Đặt

1. **Cài đặt Node.js:**
   - Tải và cài đặt Node.js từ [trang chính thức](https://nodejs.org/).

2. **Cài đặt MongoDB:**
   - Tạo tài khoản MongoDB Atlas và lấy đường dẫn kết nối.

3. **Cài đặt Mongoose:**
   - Sử dụng npm: `npm install mongoose`.

4. **Cài đặt Jest:**
   - Sử dụng npm: `npm install --save-dev jest`.

5. **Cài đặt AWS CLI:**
   - Tải và cài đặt AWS CLI từ [trang chính thức](https://aws.amazon.com/cli/).


### Sử Dụng

Dự án cung cấp một loạt các API để quản lý người dùng, sản phẩm, giỏ hàng và đơn hàng. Dưới đây là một số thao tác cơ bản:

## Quản Lý Người Dùng

1. **Đăng Ký Người Dùng:**
   - **Endpoint:** `POST /api/users/register`
   - **Mô Tả:** Đăng ký người dùng mới với thông tin cần thiết như email và mật khẩu.

2. **Đăng Nhập Người Dùng:**
   - **Endpoint:** `POST /api/users/login`
   - **Mô Tả:** Đăng nhập người dùng với email và mật khẩu.

3. **Lấy Thông Tin Người Dùng:**
   - **Endpoint:** `GET /api/users/profile`
   - **Mô Tả:** Lấy thông tin của người dùng đã đăng nhập.

## Quản Lý Sản Phẩm

4. **Lấy Danh Sách Sản Phẩm:**
   - **Endpoint:** `GET /api/products`
   - **Mô Tả:** Lấy danh sách tất cả sản phẩm có sẵn.

5. **Lấy Chi Tiết Sản Phẩm:**
   - **Endpoint:** `GET /api/products/:id`
   - **Mô Tả:** Lấy chi tiết của một sản phẩm dựa trên ID.

6. **Thêm Sửa Xóa Sản Phẩm:**
   - **Endpoint:** `POST /api/products`, `PUT /api/products/:id`, `DELETE /api/products/:id`
   - **Mô Tả:** Thêm, sửa, hoặc xóa một sản phẩm dựa trên yêu cầu.

## Quản Lý Giỏ Hàng

7. **Thêm Sản Phẩm Vào Giỏ Hàng:**
   - **Endpoint:** `POST /api/cart/add`
   - **Mô Tả:** Thêm một sản phẩm vào giỏ hàng của người dùng.

8. **Sửa Số Lượng Sản Phẩm Trong Giỏ Hàng:**
   - **Endpoint:** `PUT /api/cart/update/:productId`
   - **Mô Tả:** Sửa số lượng của một sản phẩm trong giỏ hàng.

9. **Xóa Sản Phẩm Khỏi Giỏ Hàng:**
   - **Endpoint:** `DELETE /api/cart/remove/:productId`
   - **Mô Tả:** Xóa một sản phẩm khỏi giỏ hàng.

10. **Lấy Thông Tin Giỏ Hàng:**
    - **Endpoint:** `GET /api/cart`
    - **Mô Tả:** Lấy thông tin chi tiết của giỏ hàng hiện tại.

## Quản Lý Đơn Hàng

11. **Tạo Đơn Hàng:**
    - **Endpoint:** `POST /api/orders/create`
    - **Mô Tả:** Tạo một đơn hàng mới dựa trên thông tin giỏ hàng.

12. **Lấy Thông Tin Đơn Hàng:**
    - **Endpoint:** `GET /api/orders/:orderId`
    - **Mô Tả:** Lấy thông tin chi tiết của một đơn hàng dựa trên ID.

13. **Cập Nhật Trạng Thái Đơn Hàng:**
    - **Endpoint:** `PUT /api/orders/update/:orderId`
    - **Mô Tả:** Cập nhật trạng thái của một đơn hàng dựa trên ID.
<!-- 
## Liên Kết

- [Tài liệu API](./docs/api.md)
- [Hướng dẫn Người Dùng](./docs/user-guide.md)
- [Báo Cáo Lỗi](https://github.com/your-username/your-project/issues)
- [Diễn Đàn Hỗ Trợ](https://community.your-project.com) -->
